import sys
import re
import socket
import platform
import wmi
import getpass
from subprocess import Popen, PIPE
from threading import Thread

#Functions and what not
#Get Computer Name
def get_comp_name(computer, user, password):
        c = wmi.WMI(computer=computer, user=user, password=password)
        for i in c.Win32_ComputerSystem():
                return i.Name

#Get IP Address
def get_ip(computer, user, password):
        c = wmi.WMI(computer=computer, user=user, password=password)
        for interface in c.Win32_NetworkAdapterConfiguration(IPEnabled=1):
                print "  ", interface.Description
                print "   MAC Address: ", interface.MACAddress
                for ip in interface.IPAddress:
                        print "   IP Address:  ",ip
                print ""
                        
#Get Operating System
def get_os(computer, user, password):
        c = wmi.WMI(computer=computer, user=user, password=password)
        for os in c.Win32_OperatingSystem():
                return os.Caption

#Get Physical Drive Space (Used GB / Total GB (% Free))
def get_disk_space(computer, user, password):
        c = wmi.WMI(computer=computer, user=user, password=password)
        for disk in c.Win32_LogicalDisk (DriveType=3):
            print disk.Caption, "%0.2f GB / %0.2f GB (%0.2f%% Free)" % ((long(disk.Size) - long(disk.FreeSpace))/1073741824.0, long(disk.Size)/1073741824.0,
                                                        (100.0 * long (disk.FreeSpace)/ long (disk.Size)))
#Get Used / Total Ram
def get_total_ram(computer, user, password):
        c = wmi.WMI(computer=computer, user=user, password=password)
        for ram in c.Win32_ComputerSystem():
                ram.TotalPhysicalMemory
 
                for os in c.Win32_OperatingSystem():
                        oram = "%0.1f GB / %0.1f GB" % ((long(ram.TotalPhysicalMemory)/1073741824.0) - (long(os.FreePhysicalMemory)/1048576.0),
                                                       long(ram.TotalPhysicalMemory)/1073741824.0)
                        return oram
#Get CPU Info
def get_cpu(computer, user, password):
        c = wmi.WMI(computer=computer, user=user, password=password)
        for cpu in c.Win32_Processor():
                print "   Name: ", cpu.Name
                print "   Load: ", cpu.LoadPercentage, "%"
            
#Get Shared Drives
def get_shared_drives(computer, user, password):
        c = wmi.WMI(computer=computer, user=user, password=password)
        shares = c.Win32_LogicalDisk(DriveType=4)
        if shares:
                for share in shares:
                        print share.Caption, share.ProviderName
        else:
                print "No Network Drives"   

# Main Function
def main():
        
        #Stupid ASCII Art / Header / Options
        print "####################################################"
        print "######                                        ######"
        print "######            NetUtil.py v.02             ######"
        print "######              Created By:               ######"
        print "######              Matt Morin                ######"
        print "######                                        ######"
        print "####################################################"
        print ""
        print "Options: "
        print "1. Ping Target"
        print "2. Ping Range"
        print "3. Trace Route"
        print "4. My System Info"
        print "5. Remote System Info (Windows Only)"
        print "0. Exit"
        print "---------------------------------------------------"


        #Ask user what they want to do
        function = raw_input("What would you like to do? ")
        print ""

        #If 1 is entered do PING BY IP
        if function == "1":
                
                print "Ping: Check if a specific host is up...One ping and one ping only"
        
                hname = raw_input("Target Host: ")
                ping = Popen('ping -n 1 ' + hname, stdout=PIPE)
                pipeOut = ping.stdout.read()
            
                if re.search('Received = 0', pipeOut):
                        print "Host %s could not be found, check name and try again" % hname
                        raw_input('Press Any Key to Continue')
                        main()

                elif re.search('Packets:', pipeOut):
                        ip = re.search('Reply from ([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})', pipeOut)
                        rtt = re.search('Average = (.*)ms', pipeOut)

                        print "Host %s (%s) is up.   RTT: %s ms" % (hname, ip.group(1), rtt.group(1))
                        print ""
                        raw_input('Press Any Key to Continue')
                        main()
                else:
                        print "Error finding Host %s, please try again." % hname
                        raw_input('Press Any Key to Continue')
                        main()

                    
        #If 2 is entered do PING RANGE            
        elif function == "2":
            print "Ping Range: Check an entire range of IP Addresses (1-254)"
            print "Use Format 192.168.1 (Leaving out last octet)"
            ip = raw_input("Target IP Address Range: ")

            #Check IP Range Format
            if not re.match('^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$', ip):
                print "Invalid IP Address Range format.  Please use this format:"
                print "192.168.1 (Leaving out the last octet)"
                sys.exit()

            #If valid, begin scanning range
            else:
                for i in range(0, 255):
                    ipr = ip + '.%d' % i
                    ping = Popen('ping -n 1 ' + ipr, stdout=PIPE)
                    pOut = re.search('Average = (.*)ms', ping.stdout.read())
                    if pOut:
                        print "%s is up. RTT: %s ms" % (ipr, pOut.group(1))
            raw_input('Press Any Key to Continue')
            main()
               
        #If 3 is entered do TRACE ROUTE
        elif function == "3":
            print "Trace Route: Determine number of network hops to specific target"
            ip = raw_input("Target IP Address: ")

            #Check IP Address Format
            if not re.match('^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$', ip):
                print "Invalid IP Address format.  Please use this format:"
                print "192.168.1.1"
                raw_input('Press Any Key to Continue')
                main()
                
            #If valid, begin trace route
            else:
                looper = False
                i = 0
                
                #Start loop to auto incriment TTL
                while looper == False:
                    i = i + 1
                    tParam = '%s %s' % (i, ip)
                    trace = Popen('ping -n 1 -i ' + tParam, stdout=PIPE)
                    pipeOut = trace.stdout.read()
                    tFailOut = re.search('TTL expired in transit', pipeOut)

                    #If TTL expires:
                    if tFailOut:
                            tFailIP = re.search('Reply from ([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})', pipeOut)
                            print "Hop %s: %s" % (i,tFailIP.group(1))

                    #If target is reached:
                    else:
                        tOut = re.search('TTL=', trace.stdout.read())
                        print "Hop %s: %s" % (i, ip)
                        looper = True
                        print ""
                        raw_input('Press Any Key to Continue')
                        main()

        #If 4 is entered do SYSTEM INFO
        elif function == "4":
        try:   
                print "Hostname:  ", get_comp_name("", "", "")
                print "OS:        ", get_os("", "", "")
                print "RAM:       ", get_total_ram("", "", "") 
                print ""
                print "CPU:"
                get_cpu("", "", "")
                print ""
                print "Network Interfaces:"
                get_ip("", "", "")
                print "Disk Space:"
                get_disk_space("", "", "")
                print ""
                print "Network Drives:"
                get_shared_drives("", "", "")
            
                print ""
                raw_input('Press Any Key to Continue')
                main()
        except wmi.x_wmi:
                print ""
                print "This machine is not accepting WMI queries.  Please try again."
                print ""
                raw_input('Press Any Key to Continue.')
                main()

        #If 5 is entered do REMOTE SYSTEM INFO
        elif function == "5":

                #Prompt User for Remote System Info
                print "Credentials Required to Continue"
                computer = raw_input('Target Hostname or IP Address: ')
                user = raw_input('Domain/Username of Administrative User: ')
                password = getpass.getpass()

                try:
                    #Procede getting system information
                    print "Hostname: ", get_comp_name(computer, user, password)
                    print "OS:       ", get_os(computer, user, password)
                    print "Total RAM:", get_total_ram(computer, user, password), "GB"
                    print ""
                    print "CPU:"
                    get_cpu(computer, user, password)
                    print ""
                    print "Network Interfaces:"
                    get_ip(computer, user, password)
                    print "Disk Space:    "
                    get_disk_space(computer, user, password)
                    print ""
                    print "Network Drives: "
                    get_shared_drives(computer, user, password)

                    print ""
                    raw_input('Press Any Key to Continue')
                    main()
                except wmi.x_wmi:
                        print ""
                        print "This host is either not up, not accepting WMI queries or you have entered invalid credentials.  Please try again."
                        print ""
                        raw_input('Press Any Key to Continue')
                        main()

        #If 0 is entered do EXIT
        elif function == "0":
                sys.exit()
            
        #Die if invalid
        else:
                print "Invalid Option.  Please try again."
                raw_input('Press Any Key to Continue')
                main()

main()
            

    
